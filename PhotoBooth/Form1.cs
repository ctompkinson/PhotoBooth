﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoBooth
{
    public partial class Form1 : Form
    {

        BackgroundWorker worker;   
        bool abortLoop { get; set; } 
        delegate void SetLabelDelegate(string value);
        string selectedInputPath;
        string selectedOutputPath;
        int maxImageCount = 4;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) { }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            listBox1.Items.Add(e.UserState as string);
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (worker == null)  
            {
                abortLoop = false;
                worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
                worker.DoWork += new DoWorkEventHandler(worker_DoWork); 
                worker.RunWorkerAsync();
                listBox1.Items.Add(log("Worker started."));
            }
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)  
        {
            PrintHandler printHandler = new PrintHandler();
            while (!abortLoop)
            {
                if (selectedInputPath == null) 
                {
                    worker.ReportProgress(0,log("Stopping, input path not set", "ERROR"));
                    break;
                }
                else if (selectedOutputPath == null)
                {
                    worker.ReportProgress(0,log("Stopping, output path not set", "ERROR"));
                    break;
                }
                Queue<String> filePaths = new Queue<String>(Directory.GetFiles(@selectedInputPath, "*.jpg"));
                
                List<String> outputFiles = new List<String>();
                while(filePaths.Count > 0)
                {
                    string file = filePaths.Dequeue();
                    string fileName = Path.GetFileName(file);
                    string outputFile = selectedOutputPath + '\\' + fileName;
                    worker.ReportProgress(0, log("Moving " + file + " to " + outputFile));
                    File.Move(file, outputFile);


                    worker.ReportProgress(0, log("Sending file to print: " + file));
                    outputFiles.Add(outputFile);
                    if (outputFiles.Count == this.maxImageCount)
                    {
                        printHandler.printImage(outputFiles.ToArray());
                        outputFiles.Clear();
                    }

                    if (abortLoop) { break; }
                }
                if (outputFiles.Count > 0)
                {
                    printHandler.printImage(outputFiles.ToArray());
                }
            }
            clearWorker();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            if (worker == null)
            {
                this.Close();
            }
            else
            {
                listBox1.Items.Add(log("Cannot exit, worker is still running.", "ERROR"));
            }
        }

        private void openFolder_Button_Click(object sender, EventArgs e)
        {
            selectedInputPath = selectFolder();
        }

        private void openOutputFolder_Button_Click(object sender, EventArgs e)
        {
            selectedOutputPath = selectFolder();
        }

        private string selectFolder()
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog(this) == DialogResult.OK)
            { return fbd.SelectedPath; }
            else { return ""; }
        }

        private string log(string message, string level = "INFO")
        {
            string now = DateTime.Now.ToString("dd/MM/yy h:mm:ss");
            return now + " " + level + " " + message;
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            abortLoop = true;
        }

        private void clearWorker()
        {
            worker.ReportProgress(0, log("Worker stopped."));
            worker = null;
        }
    }
}
