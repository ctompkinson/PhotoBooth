﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace PhotoBooth
{
    class PrintHandler
    {

        int maxImageWidth;
        int maxImageHeight;

        int imageDrawPointX = 100;
        int imageDrawPointY = 100;

        private static int A4_BORDER_WIDTH = 50;
        private static int A4_WIDTH = 4960;
        private static int A4_HEIGHT = 7016;

        int gap = 150;

        public bool printImage(string[] imagePaths)
        {
            
            // Get desired image width
            int desiredImageWidth;
            if (imagePaths.Count() == 1)
            {
                desiredImageWidth = (A4_WIDTH - (gap + 2 * A4_BORDER_WIDTH)); 
            }
            else
            {
               desiredImageWidth = (A4_WIDTH - ((gap + 2 * A4_BORDER_WIDTH) / 2));
            }

            try
            {
                List<Image> images = new List<Image>();
                foreach (string imagePath in imagePaths)
                {
                    Image image = Image.FromFile(imagePath);
                    image = this.Scale(image, maxImageWidth, A4_HEIGHT);
                    images.Add(image);
                }
                
                PrintDocument pd = new PrintDocument();
                pd.PrintPage += (sender, e) => printDoc_PrintPage(images, sender, e);
                pd.Print();
            }
            catch (System.Drawing.Printing.InvalidPrinterException)
            {
                return false;
            }

            return true;
        }

        private void printDoc_PrintPage(List<Image> images, object sender, PrintPageEventArgs e)
        {

            if (images.Count > 4)
            {
                throw new Exception(); // TODO: Make less shit 
            }

            // Set image points
            Point ulCorner = new Point(PrintHandler.A4_BORDER_WIDTH,PrintHandler.A4_BORDER_WIDTH);
            Point urCorner = new Point(images.First().Width + PrintHandler.A4_BORDER_WIDTH + gap, PrintHandler.A4_BORDER_WIDTH);
            Point llCorner = new Point(ulCorner.X, (images.First().Width + gap + PrintHandler.A4_BORDER_WIDTH ));
            Point lrCorner = new Point(urCorner.X, llCorner.Y);
            List<Point> points = new List<Point>();
            points.Add(ulCorner);
            points.Add(urCorner);
            points.Add(llCorner);
            points.Add(lrCorner);

            foreach (Image image in images)
            {
                e.Graphics.DrawImage(image, points.First());
                points.RemoveAt(0);
            }
        }

        private int getDesiredImageWidth() 
        {
            return A4_WIDTH - ((gap + 2 * A4_BORDER_WIDTH) / 2);
        }

        private Image Scale(Image img, int maxWidth, int maxHeight)
        {
            double scale = 1;

            if (img.Width > maxWidth || img.Height > maxHeight)
            {
                double scaleW, scaleH;

                scaleW = maxWidth / (double)img.Width;
                scaleH = maxHeight / (double)img.Height;

                scale = scaleW < scaleH ? scaleW : scaleH;
            }

            return this.Resize(img, (int)(img.Width * scale), (int)(img.Height * scale));
        }

        private Image Resize(Image img, int width, int height)
        {
            return img.GetThumbnailImage(width, height, null, IntPtr.Zero);
        }

    }
}
